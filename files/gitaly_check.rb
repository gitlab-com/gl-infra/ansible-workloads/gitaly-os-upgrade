#!/usr/bin/env ruby

# Usage: gitaly_check.rb <server_name>

Gitlab.config.repositories.storages['default']['gitaly_address'] = "#{ARGV[0]}" # Point to test server
Gitlab::GitalyClient.clear_stubs! # Force the new configuration go into effect
fail unless Gitlab::HealthChecks::GitalyCheck.check('default').success
puts Gitaly::Server.new('default').send(:info).storage_statuses.map(&:filesystem_id).first
