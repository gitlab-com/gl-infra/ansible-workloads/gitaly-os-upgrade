#!/bin/bash

set -euo pipefail
#set -x

# config

gcp_project="$1"
instance="$2"

dry_run=${DRY_RUN:-false}
disk_size=40GB
disk_type=pd-ssd

# lib

wait_for_input() {
  declare input
  while read -n 1 -p "Continue (y/n): " input && [[ $input != "y" && $input != "n" ]]; do
    echo
  done
  if [[ $input != "y" ]]; then
    echo
    echo >&2 "error: aborting"
    exit 1
  fi
  echo
}

RED='\033[0;31m'
NC='\033[0m'

# prep

instance_uri="$(gcloud --project=$gcp_project --quiet compute instances list --filter "name~^$instance$" --uri)"
zone="$(gcloud --project=$gcp_project --quiet compute instances describe $instance_uri --format json | jq -r '.zone')"
disk_name="$instance"
snapshot_prefix="os-upgrade-$instance"
snapshot_uri="$(gcloud --project=$gcp_project --quiet compute snapshots list --filter "name:($snapshot_prefix)" --sort-by '~creationTimestamp' --limit 1 --uri)"

dry_run_cmd=
if [[ $dry_run = 'true' ]]; then
  dry_run_cmd=echo
fi

if [[ -z $instance_uri ]]; then
  echo >&2 "error: instance_uri is empty"
  exit 1
fi
if [[ -z $zone ]]; then
  echo >&2 "error: zone is empty"
  exit 1
fi
if [[ -z $snapshot_uri ]]; then
  echo >&2 "error: snapshot_uri is empty"
  exit 1
fi

# confirm

echo "about to perform rollback"
echo
echo "instance_uri=$instance_uri"
echo "zone=$zone"
echo "snapshot_uri=$snapshot_uri"
echo "dry_run=$dry_run"
echo

if [[ $dry_run != 'true' ]]; then
  echo -e "${RED}WARNING: this will rebuild the box!${NC}"
  echo "please confirm: this look good to you?"
  echo
  wait_for_input
  echo
fi

# run

$dry_run_cmd gcloud --project=$gcp_project --quiet compute instances stop $instance_uri
$dry_run_cmd gcloud --project=$gcp_project --quiet compute instances detach-disk $instance_uri --disk=$disk_name || true
$dry_run_cmd gcloud --project=$gcp_project --quiet compute disks delete $disk_name --zone=$zone
$dry_run_cmd gcloud --project=$gcp_project --quiet compute disks create $disk_name --type=$disk_type --size=$disk_size --zone=$zone --source-snapshot=$snapshot_uri --guest-os-features=UEFI_COMPATIBLE
$dry_run_cmd gcloud --project=$gcp_project --quiet compute instances attach-disk $instance_uri --disk=$disk_name --zone=$zone --boot
$dry_run_cmd gcloud --project=$gcp_project --quiet compute instances set-disk-auto-delete $instance_uri --disk=$disk_name
$dry_run_cmd gcloud --project=$gcp_project --quiet compute instances start $instance_uri
