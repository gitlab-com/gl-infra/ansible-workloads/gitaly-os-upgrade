# Gitaly OS Upgrade setup

## Setup

```
cp group_vars/all.yml.example group_vars/all.yml
pip3 install -r requirements.txt
```

Set the environment variable `GITLAB_OPS_API_TOKEN` to your personal API token
for ops.gitlab.net

## Usage

For each invocation you'll need to specify the following:

- Environment
- Batch

For example:

```
bin/rebuild gstg 2
```
